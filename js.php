<script>
if(screen.width>900){
	alert('This game is for mobile only since it uses touch-events');
	location.replace('http://jeahar11.dreamhosters.com/');
}
var western_guit=new Howl({
	src: ['/sites/games/lucky/sounds/western_guitare.mp3'],
	autoplay: true,
	loop: true,
	html5: true,
	volume: 1	
});
western_guit.play();
gid("game_header").onclick = function(){window.location.reload();};
var taille=0;
var number=0;
var distance=0;
var score=0;
var playing_boolean=true;
var challenge_boolean=0;
var challenge_count=0;
var touch_point_x = 0;
var touch_point_y = 0;
var move_x = 0;
var move_y = 0;
var gunshotsound = new Audio('sounds/fit_gunshot.mp3');
var shell_Fall = new Audio('sounds/Shells_falling.mp3');
var ricoch = new Audio('sounds/ricochet.mp3');
var loading = new Audio('sounds/gun_reload.mp3');
var aim_x = 50;
var aim_y = 50;
var colt = gid('gun_pic');
var cross = gid('cross');
var x_view = 1680;
var y_view = -630;
var target = 0;
var target_x = 0;
var target_xpos = 0;
var target_bottomPos = 0;
var target_y = 0;
var start_time=0;
var end_time=0;
var bullets=0;
document.documentElement.style.backgroundPosition = -.2 * x_view + "vw " + .1 * y_view + "vh";
cross.style.transition="1s";
function set_origin()
{
	if(playing_boolean==true){
		touch_point_x = event.touches[0].clientX;
	  	touch_point_y = event.touches[0].clientY;
		loading.play();
	}
}
function show_crossCoordinates()
{
	gid("game_header").innerHTML = Math.floor(x_view) + ", " + Math.floor(y_view);
}
function trackme()
{
	if(playing_boolean==true){
		move_x = event.touches[0].clientX - touch_point_x;
 		move_y = event.touches[0].clientY - touch_point_y;
		x_view += (.1 * move_x);
		y_view -= (.1 * move_y);
		if(x_view > 3481)x_view = 3481;
		if(y_view > 0)y_view = 0;
		if(x_view < 0)x_view = 0;
		if(y_view < -1190)y_view = -1190;
		document.documentElement.style.backgroundPosition = -.2 * x_view + "vw " + .1 * y_view + "vh";
		target.style.left = target_xpos - x_view + "px";
		target.style.bottom = target_bottomPos - y_view + "px";
		var this_hit_box=gid('hit_pts_box_'+number);
		this_hit_box.style.left=this_hit_box.xpos-x_view+"px";
		gid('hit_pts_box_'+number).style.bottom=gid('hit_pts_box_'+number).bottomPos-y_view+"px";
	}
}
function hit(){
	var this_hit_box=gid("hit_pts_box_"+number);
	this_hit_box.down=1;
	this_hit_box.innerHTML="+"+Math.floor(accuracy_points);
	score+=accuracy_points;
	this_hit_box.style.transition='.7s';
	this_hit_box.style.animation='intro .4s linear';
	this_hit_box.xpos=target_xpos;
	this_hit_box.bottomPos=target_bottomPos+taille*4;
	this_hit_box.style.left=60+"px";
	this_hit_box.style.fontSize=30+"px";
	this_hit_box.style.textShadow="-2px 3px 4px black";
	this_hit_box.style.bottom=this_hit_box.bottomPos-y_view+"px";
	var this_target=gid('target_'+number);
	this_target.style.transition=0.4+"s";
	this_target.style.left=Math.random()*100+"vw";
	this_target.style.bottom=100+"vh";
	this_target.style.transform="translate(-50%,-50%) rotateX("+Math.random()*60000+"deg) rotateY("+Math.random()*60000+"deg) scale(0)";
	setTimeout(function(){
		this_hit_box.style.bottom='30px';
		this_hit_box.style.animation='fadeout .7s linear';
	},800);
	setTimeout(function(){
		this_hit_box.innerHTML='';
		gid('score').innerHTML=Math.floor(score);
		this_hit_box.style.display='none';
		this_target.remove();
	},1500);
	if(challenge_count==9){
		end_round();
	}
	challenge_count+=1;
	generateTarget();
}
function shoot()
{
	if(playing_boolean==true){
		bullets+=1;
		gunshotsound.play();
		target_x = parseInt(target.style.left,10) - (screen.width/2);
		target_y = target.style.bottom;
		colt.style.transform = 'rotateY(50deg) rotateZ(40deg)';
		var distance=Math.sqrt(Math.pow((target.offsetLeft-cross.offsetLeft),2)+Math.pow((target.offsetTop - cross.offsetTop),2));
		accuracy_points=100-distance/((190)/2)*100;
		if(accuracy_points===Infinity){
			accuracy_points=100;
		}
		if(distance<(taille+20)/2){
			hit();
		}else{
			ricoch.play();
		}
		setTimeout(function(){
			colt.style.transform = 'rotateY(50deg) rotateZ(10deg)';
		},200);
		setTimeout(function(){
			shell_Fall.play();
		},Math.random()*3000);
	}
}
var stats=document.createElement('div');
gid('main').append(stats);
stats.id='stats';
stats.style.width="80%";
stats.style.height="80%";
stats.style.textAlign="center";
stats.style.display="none";
stats.style.flexDirection="column";
stats.style.justifyContent="space-around";
stats.style.backgroundColor="rgba(1,1,1,.7)";
stats.style.color="orange";
stats.style.textAlign="left";
stats.style.position='absolute';
stats.style.left='50%';
stats.style.border='solid 5px black';
stats.style.borderRadius='2vh';
stats.style.top='50%';
stats.style.transform='translate(-50%,-50%)';
stats.style.overflow='hidden';
var replay_button=document.createElement('div');
replay_button.style.width="min-content";
replay_button.style.padding="3vh";
replay_button.style.height="min-content";
replay_button.style.textAlign="center";
replay_button.style.flexDirection="column";
replay_button.style.justifyContent="center";
replay_button.style.backgroundColor="transparent";
replay_button.style.color="yellow";
replay_button.innerHTML='retry';
replay_button.style.margin='0 auto';
replay_button.style.border='solid 5px black';
replay_button.style.borderRadius='2vh';
replay_button.style.bottom='0';
replay_button.style.display='none';
replay_button.onclick=function(){
	stats.style.animation='shrink .3s linear';
	stats.innerHTML='';
	setTimeout(function(){
		stats.style.display='none';
	},300);
	new_round();
}

function end_round(){
	playing_boolean=false;
	var chrono=new Date();
	end_time=chrono.getTime();
	var time_taken=end_time-start_time;
	var this_hit_box=gid("hit_pts_box_"+number);

	var speed=Math.round(time_taken/100)/number;
	var accuracy=Math.floor(score/number);
	if(accuracy>luckyaccuracy){
		luckyaccuracy=accuracy;
		setCookie(luckyaccuracy);
	}
	if(speed<luckyspeed){
		luckyspeed=speed;
		setCookie(luckyspeed);
	}
	if(bullets<luckybullets){
		luckybullets=bullets;
		setCookie(luckybullets);
	}
	if(number>luckynumber){
		luckynumber=number;
		setCookie(luckynumber);
	}
	stats.innerHTML="\
		Hits: "+number+" (Your best is "+luckynumber+")<br>\
		Speed: "+speed+"s (Your best is "+luckyspeed+"s)<br>\
		Accuracy: "+accuracy+"% (Your best is "+luckyaccuracy+"%)<br>\
		Bullets used: "+bullets+" (Your best is "+luckybullets+")<br>"
	;
	var trucs_muches=gid('main').children;
	for(i=0;i<trucs_muches.length;i++){
		trucs_muches[i].style.display='none';
	}
	gid('score').style.display='none';
	stats.style.animation='intro .2s linear';
	stats.style.display='flex';
	stats.append(replay_button);
	replay_button.style.display='flex';
}
function new_round(){
	number=0;
	cross.style.display='none';
	colt.style.display='none';
	playing_boolean=true;
	challenge_count=0;
	score=0;
	bullets=0;
	var hit_boxes=document.getElementsByClassName("hit_pts_box");
	for(i=0;i<hit_boxes.length;i++){
		hit_boxes[i].style.display='none';
	}
	var trucs_muches=gid('main').children;
	for(i=0;i<trucs_muches.length;i++){
		trucs_muches[i].style.display='block';
	}
	stats.style.display='none';
	gid('score').style.display='block';
	generateTarget();
}
function generateTarget()
{
	if(playing_boolean==true){
		setTimeout(function(){
			cross.style.display='block';
			colt.style.display='block';
			if(number==1){
				var chrono=new Date();
				start_time=chrono.getTime();
			}
		},200);
		number++;
		taille=Math.random()*170+40;
		target = document.createElement("img");
		target.src = "images/target.png";
		target.style.position = "absolute";
		cross.style.transform="translate(-50%,-50%) scale("+(taille/80)+")";
		target.style.height = taille + "px";
		target.style.width= taille + "px";
		target.style.boxShadow="0 0 10px black";
		target.style.borderRadius="100%";
		target_bottomPos = 70*Math.random()-150;
		target_xpos = 100*Math.random() + 1840; 
		target.style.bottom = target_bottomPos - y_view + "px";
		target.style.left = target_xpos - x_view + "px";
		target.style.transform = "translate(-50%,-50%)";
		target.style.animation="intro linear 4s";
		target.id="target_"+number;
		gid("game_display").append(target);
		var hit_pts_box=document.createElement('div');
		hit_pts_box.id="hit_pts_box_"+number;
		gid('main').append(hit_pts_box);
		var this_hit_box=gid('hit_pts_box_'+number);
		this_hit_box.style.position='fixed';
		this_hit_box.style.fontSize=taille;
		this_hit_box.xpos=target_xpos;
		this_hit_box.bottomPos=target_bottomPos;
		this_hit_box.style.left=this_hit_box.xpos-x_view+"px";
		this_hit_box.style.bottom=this_hit_box.bottomPos-y_view+"px";
		hit_pts_box.className='hit_pts_box';
		hit_pts_box.down=0;
	}	
}
function clear_lucky_cookies(){
	deleteCookie(luckynumber);
	deleteCookie(luckybullets);
	deleteCookie(luckyspeed);
	deleteCookie(luckyaccuracy);
	deleteCookie(luckyname);
	deleteCookie(first_visit);
}
var luckyaccuracy=0;
var luckyname='visitor';
var luckynumber=0;
var luckyspeed=10000000;
var luckybullets=1000000;
if(isCookieSet('luckyaccuracy')){
	luckyaccuracy=getCookie('luckyaccuracy');
}
if(isCookieSet('luckynumber')){
	luckynumber=getCookie('luckynumber');
}
if(isCookieSet('luckybullets')){
	luckybullets=getCookie('luckybullets');
}
if(isCookieSet('luckyname')){
	luckyname=getCookie('luckyname');
	alert('Hi '+luckyname);
}else{
	alert('Hello cowboy');
}
new_round();
setInterval(function(){
	var distance=Math.sqrt(Math.pow((target.offsetLeft-cross.offsetLeft),2)+Math.pow((target.offsetTop - cross.offsetTop),2));
	if(distance<(taille/2)/4){
		cross.style.backgroundImage="url('images/cross_red.png')";
		cross.style.transition="0s";
	}else if(distance<(taille/2)/1.4){
		cross.style.backgroundImage="url('images/cross_green.png')";
		cross.style.transition="0s";
	}else{
		cross.style.transition="0s";
		cross.style.backgroundImage="url('images/cross_black.png')";
	}
	cross.style.transition="1s";
},100);
</script>
