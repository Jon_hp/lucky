<style>
@keyframes intro{
	from{width:0;height:0;font-size:0;transform:translate(-50%,-50%);}
}
body{
overflow-y:hidden;
}
#score{
	width:150px;
	height:60px;
	color:rgba(250,200,10,1);
	font-size:50px;
	position:fixed;
	left:10px;
	bottom:30px;
	text-shadow:-2px 2px 1px black;
}
.hit_pts_box{
	width:min-content;
	height:min-content;
	color:rgba(250,200,10,1);
	font-size:60px;
	position:fixed;
	left:50%;
	bottom:65%;
	transform:translateX(-50%);
	text-shadow:-2px 2px 1px black;
	text-align:center;
}

#main
{
	width:100vw;
	max-width:1000px;
	height:100vh;
	position:fixed;
	top:0;
	left:0;
}
#game_header
{
	color:black;
	width:100%;
	text-align:center;
	border:solid 1px black;
	border-radius:3vh;
	box-shadow:-1vh 1vh 1vh black;
	font-size:14vw;
	flex:1;
	font-family:'CombatRegular';
  	font-weight:normal;
  	font-style:normal; 
	background:url("images/wood_texture.jpg");
	opacity:.9;
	text-shadow:0 0 1vh white;
	display:flex;
	flex-direction:column;
	justify-content:center;
}
#cross
{
	background:url("cross_black");
	background-size:cover;
	width:100px;
	height:100px;
	transform:translate(-50%,-50%);
	position:absolute;
	left:50%;
	bottom:37%;
	border-radius:100%;
	z-index:1;
	animation:appear .5s linear;
	display:none;
}
#game_display
{
	flex:5;
}
#game_controls
{
	flex:2;
}
#character
{
	flex:1;
	height:100%;
}
#touch
{
	flex:1;
	height:100%;
}
#character
{
	overflow:visible;
}
#gun_pic
{
	display:none;
	position:absolute;
	bottom:0;
	right:-20vw;
	width:70vw;
	transform:rotateY(50deg) rotateZ(10deg);
	animation:raise .3s ease-in;
}
@keyframes raise{
	from{
		transform:translateY(160px);
	}
}
:root
{
	background:url("images/city_west.jpg");
	background-size:800%;
	background-position:0vw 0vh;
	background-repeat:no-repeat;
	background-color:rgba(60,40,0,1);
	overflow:visible;
}
.target
{
	background:url("target.png");
	background-size:cover;
	position:absolute;
}
@keyframes appear{
	from{opacity:0;font-size:0px;transform:translate(-50%,-50%)}
}
@keyframes fadeout{
	to{opacity:0;font-size:50px;}
}
@keyframes flips{
	from{transform:rotateX(0deg) translate(-50%,-50%);}
	to{transform:rotateX(-1080deg) translate(-50%,-50%);}
}
@keyframes shrink{
	to{width:0;height:0;font-size:0;}
}
</style>

