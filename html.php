<html>
<head>
<title>Lucky</title>
<script src="/lib/howler.js/dist/howler.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="keywords" content="">
</head>
<body>
<div id=main class="flex-vert-sb" ontouchstart="set_origin()" ontouchmove="trackme()" ontouchend="shoot()">
	<div id=game_header class='flex-hor-center'>
		Lucky	
	</div>
	<div id=game_display>
		<div id=cross>
		</div>
	</div>
	<div class='flex-hor-sb' id=game_controls>
		<div id=character>
		</div>
		<div id=touch >
			<img id=gun_pic src="images/handGun.gif">
		</div>
	</div>
	<div id=score>
	</div>
</div>
<div id=spacer></div>
</body>
</html>

